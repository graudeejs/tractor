asm= avra

main: tractor.asm
	$(asm) tractor.asm

program: tractor.hex
	avrdude -p m8 -c usbasp -e -U flash:w:tractor.hex

clean:
	rm -f tractor.cof
	rm -f tractor.hex
	rm -f tractor.obj
