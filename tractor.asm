; vim:set tabstop=8 shiftwidth=8:

; By Aldis Berjoza <killasmurf86@gmail.com>
; October 8, 2009


.include "include/m8def.inc"
.include "include/robot.inc"

; CSEG {{{1
.cseg
start:
; INTERRUPT VECTORS {{{2
	; page 44
	rjmp	stack_init	; reset
	rjmp	INT_DUMMY_	; int 0
	rjmp	INT_DUMMY_	; int 1
	rjmp	INT_DUMMY_	; timer2 compare match
	rjmp	INT_DUMMY_	; timer2 overflow
	rjmp	INT_DUMMY_	; timer1 capture event
	rjmp	INT_DUMMY_	; timer1 compare match a
	rjmp	INT_DUMMY_	; timer1 compare match b
	rjmp	INT_DUMMY_	; timer1 Counter overflow
	rjmp	INT_DUMMY_	; timer1 overflow
	rjmp	INT_DUMMY_	; timer0 overflow
	rjmp	INT_DUMMY_	; serial transfer complete
	rjmp	INT_DUMMY_	; usart rx complete
	rjmp	INT_DUMMY_	; usard data registrly empty
	rjmp	INT_DUMMY_	; usart tx complete
	rjmp	INT_ADC		; adc conversion complete
	rjmp	INT_DUMMY_	; EEPROM ready
	rjmp	INT_DUMMY_	; Analog comparator
	rjmp	INT_DUMMY_	; Two-wire serial interface
	rjmp	INT_DUMMY_	; store program memory ready
; 2}}}

;INT_ADC {{{2
INT_ADC:
	push	r16
	in	r16, SREG
	push	r16

	ldi	r16, 0xff
	rcall	LED_SHOW_NUMBER

	pop	r16
	out	SREG, r16
	pop	r16
	reti
; 2}}}

; INIT {{{2
stack_init:
	cli		; Disable interrupts

	ldi	r16, high(RAMEND)
	ldi	r17, low(RAMEND)
	out	SPH, r16
	out	SPL, r17

init:

	; Configure input ports
	in	r16, SENSOR_LINE_PORT
	in	r17, SENSOR_LINE_DDR
	cbr	r16, SENSOR_LINE_MASK
	cbr	r17, SENSOR_LINE_MASK
	out	SENSOR_LINE_PORT, r16
	out	SENSOR_LINE_DDR, r17
	nop

	in	r16, SENSOR_DISTANCE_PORT
	in	r17, SENSOR_DISTANCE_DDR
	cbr	r16, SENSOR_DISTANCE_MASK
	cbr	r17, SENSOR_DISTANCE_MASK
	out	SENSOR_DISTANCE_PORT, r16
	out	SENSOR_DISTANCE_DDR, r17
	nop

	in	r16, BUTTON_PORT
	in	r17, BUTTON_DDR
	sbr	r16, BUTTON_MASK
	cbr	r17, BUTTON_MASK
	out	BUTTON_PORT, r16
	out	BUTTON_DDR, r17
	nop

	; Configure output ports
	in	r16, MOTOR_PORT
	in	r17, MOTOR_DDR
	cbr	r16, MOTOR_MASK
	sbr	r17, MOTOR_MASK
	out	MOTOR_PORT, r16
	out	MOTOR_DDR, r17
	nop

	in	r16, MOTOR_ENABLE_PORT
	in	r17, MOTOR_ENABLE_DDR
	cbr	r16, MOTOR_ENABLE_MASK
	sbr	r17, MOTOR_ENABLE_MASK
	out	MOTOR_ENABLE_PORT, r16
	out	MOTOR_ENABLE_DDR, r17
	nop

	in	r16, LED_PORT
	in	r17, LED_DDR
	cbr	r16, LED_MASK
	sbr	r17, LED_MASK
	out	LED_PORT, r16
	out	LED_DDR, r17
	nop

	;Configure interrupt ports
	in	r16, INT_PORT
	in	r17, INT_DDR
	cbr	r16, INT_MASK
	cbr	r17, INT_MASK
	out	INT_PORT, r16
	out	INT_DDR, r17
	nop

	;	Interrupts disabled, beause of how sensors work currently
;	; Configure interrupt ports
;	in	r16, GICR
;	sbr	r16, 1<<INT0 	; enable int 0
;	out	GICR, r16
;
;	in	r16, MCUCR
;	sbr	r16, 1<<ISC11 	; change on pin will cause intrerrupt. Page 64
;	out	MCUCR, r16

	; Configure ADC
	in	r16, ADMUX		; Page 202
	cbr	r16, (1<<REFS1) | (1<<MUX3) | (1<<MUX2) | (1<<MUX1) | (1<<MUX0)
	sbr	r16, (1<<REFS0) | (1<<ADLAR)  ;| (1<<MUX0)			; REFS1=0 & REFS0=1 sets reference voltage to AVCC
	out	ADMUX, r16
	nop
	in	r16, ADCSRA
	sbr	r16, (1<<ADEN) | (1<<ADPS2) | (1<<ADIE) | (1<<ADFR)	; Enable Analog to Digital converter in Free Running Mode, Enable Interrupt, + Page 205
	out	ADCSRA, r16
	nop
	in	r16, ADCSRA
	sbr	r16, 1<<ADSC
	out	ADCSRA, r16



	sei		;Enable Interruots
; 2}}}

; MAIN {{{2
main:



;sharp_test:
;
;
;	in	r16, ADCH
;	lsr	r16, 2
;;	eor	r31, r31
;	clr	r31
;;	eor	r30, r30
;	clr	r30
;	ldi	r29, 255
;	rcall	WAIT
;
;	lsl	r16, 3
;	rcall	LED_SHOW_NUMBER
;
;
;
;	rjmp	sharp_test


main_wait_button:
	sbis	BUTTON_PIN, BUTTON0_P
	rjmp	main_button_click
	rjmp	main_wait_button

main_button_click:
	eor	r31, r31
	ldi	r30, 0x10
	eor	r29, r29
	call	WAIT	; Wait 5s
	ldi	r17, MOTOR_ENABLE_BOUGH
	rcall	MOTOR_ENABLE

	eor	r31, r31
	ldi	r30, high(ROTATE_FAST_360)
	ldi	r29, low(ROTATE_FAST_360)
	ldi	r17, GO_RIGHT_FAST
	rcall	ROTATE

	ldi	r17, GO_STRAIGHT
	rcall	MOTOR_ROTATE
	
	ldi	r22, 1
		
main_loop:
	sbis	BUTTON_PIN, BUTTON0_P
	rjmp	main_button_click
	; check which sensors are activated
	rcall	SENSOR_LINE_READ
	or	r16, r16
	breq	not_on_line
	rcall	ON_LINE

not_on_line:
	
	rjmp	main_loop
; 2}}}

; ON_LINE and INT_0 {{{2
INT_0_:
ON_LINE:
;===============================
; Perform acrions if Robot is on white line
;==============================
; INPUT: 
;	r16 - 
;
; Label numbers represend state of sensors: BL BR FL FR
;===============================
	push	r19
	push	r17
	push	r16
	rcall	SENSOR_LINE_READ

on_line_change:
	mov	r19, r16
	rcall	LED_SHOW_NUMBER

; Special cases
on_line_0000:
	or	r16, r16
	brne	on_line_0001
on_line_exit:
	pop	r16
	pop	r17
	pop	r19
	ret

; One sensor active
on_line_0001:
	cpi	r16, 0b0001
	brne	on_line_0010
	ldi	r17, GO_BACK
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_0010:
	cpi	r16, 0b0010
	brne	on_line_0100
	ldi	r17, GO_BACK
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_0100:
	cpi	r16, 0b0100
	brne	on_line_1000
	ldi	r17, GO_STRAIGHT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_1000:
	cpi	r16, 0b1000
	brne	on_line_0011
	ldi	r17, GO_STRAIGHT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait


; Two sensors active
on_line_0011:
	cpi	r16, 0b0011
	brne	on_line_1100
	ldi	r17, GO_BACK
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_1100:
	cpi	r16, 0b1100
	brne	on_line_0101
	ldi	r17, GO_STRAIGHT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_0101:
	cpi	r16, 0b0101
	brne	on_line_1010
	ldi	r17, GO_RIGHT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_1010:
	cpi	r16, 0b1010
	brne	on_line_1110
	ldi	r17, GO_LEFT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

; exceptions
on_line_1110:
	cpi	r16, 0b1110
	brne	on_line_1101
	ldi	r17, GO_RIGHT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_1101:
	cpi	r16, 0b1101
	brne	on_line_1011
	ldi	r17, GO_RIGHT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_1011:
	cpi	r16, 0b1011
	brne	on_line_0111
	ldi	r17, GO_LEFT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_0111:
	cpi	r16, 0b0111
	brne	on_line_1001
	ldi	r17, GO_LEFT
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_1001:
	cpi	r16, 0b1001
	brne	on_line_0110
	ldi	r17, GO_LEFT_FAST
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait

on_line_0110:
	cpi	r16, 0b0110
	brne	on_line_1111
	ldi	r17, GO_RIGHT_FAST
	rcall	MOTOR_ROTATE
	rjmp	on_line_wait


on_line_1111:
;	cpi	r16, 0b1111
	rjmp	on_line_error

on_line_error:
	sbr	r16, 0b10000
	rcall	LED_SHOW_NUMBER
	ldi	r17, MOTOR_ENABLE_BOUGH
	rcall	MOTOR_DISABLE
	rjmp	on_line_exit

on_line_wait:
	rcall	SENSOR_LINE_READ
	cpse	r16, r19
	rjmp	on_line_change
	jmp	on_line_wait

; 2}}}

; ROTATE {{{2
ROTATE:
;===============================
; FAST ROTATE
;==============================
; INPUT:
;	r17, direction (GO_RIGHT, GO_LEFT)
;	...
;==============================
	rcall	MOTOR_ROTATE
	rcall	WAIT
	ret
; 2}}}

; SENSOR_LINE_READ {{{2
SENSOR_LINE_READ:
;===============================
; read line sensors, and return status in r16
;==============================
; RETURNS:
;	r16
;		bit 0 - FR sensor
;		bit 1 - FL sensor
;		Bit 2 - BR sensor
;		bit 3 - BL sensor
;=============================
; 0x00100000 is ~5 sec
;==============================
	clr	r16
	sbic	SENSOR_LINE_PIN, SENSOR_LINE_FR_P
	sbr	r16, 0b0001
	sbic	SENSOR_LINE_PIN, SENSOR_LINE_FL_P
	sbr	r16, 0b0010
	sbic	SENSOR_LINE_PIN, SENSOR_LINE_BR_P
	sbr	r16, 0b0100
	sbic	SENSOR_LINE_PIN, SENSOR_LINE_BL_P
	sbr	r16, 0b1000
	ret
; 2}}}

; WAIT {{{2
WAIT:
;===============================
; wait r31:r30:r29:00 cycles
; RETURN:
;	r31 = 0
;	r30 = 0
;	r29 = 0
;==============================
	push	r28
	clr	r28
wait_1:
	or	r28, r28
	breq	wait_2
	dec	r28
	rjmp	wait_1
wait_2:
	or	r29, r29
	breq	wait_3
	dec	r29
	dec	r28
	rjmp	wait_1
wait_3:
	or	r30, r30
	breq	wait_4
	dec	r30
	dec	r29
	dec	r28
	rjmp	wait_1
wait_4:
	or	r31, r31
	breq	wait_exit
	dec	r31
	dec	r30
	dec	r29
	dec	r28
	rjmp	wait_1
wait_exit:
	pop	r28
	ret
; 2}}}

; INT_DUMMY_ {{{2
INT_DUMMY_:
;===============================
; Dummy interrupt
;==============================
	reti
; 2}}}

; MOTOR_ROTATE {{{2
MOTOR_ROTATE:
;===============================
; Motor Rotate
;==============================
; INPUT: 
;	r17 - Direction of motors to rotate
;===============================
	push	r16
	in	r16, MOTOR_PORT
	cbr	r16, GO_MASK
	or	r16, r17
	out	MOTOR_PORT, r16
	pop	r16
	ret
; 2}}}

; MOTOR_REVERSE {{{2
MOTOR_REVERSE:									; seams buggy need to investigate
;===============================
; Reverse bough motors
;==============================
	push	r16
	push	r17
;	ldi	r17, MOTOR_UMASK
	in	r16, MOTOR_PORT
;	or	r16, r17
;	eor	r16, r17
	cbr	r16, MOTOR_UMASK

	ldi	r17, MOTOR_MASK
	eor	r16, r17
	out	MOTOR_PORT, r16
	pop	r17
	pop	r16
	ret
; 2}}}

; MOTOR_DISABLE {{{2
MOTOR_DISABLE:
;===============================
; Disablw Motor/Motors
;==============================
; INPUT: 
;	r17 - Motors to enable
;===============================
	push	r16
	in	r16, MOTOR_ENABLE_PORT
	or	r16, r17
	eor	r16, r17
	out	MOTOR_ENABLE_PORT, r16
	pop	r16
	ret
; 2}}}

; MOTOR_ENABLE {{{2
MOTOR_ENABLE:
;===============================
; Enable Motor/Motors
;==============================
; INPUT: 
;	r17 - Motors to enable
;===============================
	push	r16
	in	r16, MOTOR_ENABLE_PORT
	or	r16, r17
	out	MOTOR_ENABLE_PORT, r16
	pop	r16
	ret
; 2}}}

; MOTOR_ENABLE_SWITCH {{{2
MOTOR_ENABLE_SWITCH:
;===============================
; Switch Motor/Motors Enable
;==============================
; INPUT: 
;	r17 - Motors to enable
;===============================
	push	r16
	in	r16, MOTOR_ENABLE_PORT
	eor	r16, r17
	out	MOTOR_ENABLE_PORT, r16
	pop	r16
	ret
; 2}}}

; LED_SHOW_NUMBER {{{2
LED_SHOW_NUMBER:
;===============================
; Turns on Led/Leds as needed to show 4bit number
;==============================
; INPUT: 
;	r16 - number to show
; LED MEANING in BITS:
;	GREEN 0	= 0
;	RED 0	= 1
;	GREEN 1	= 2
;	RED 0	= 3
; If numver higher than 15 is requested, then
;	LED_ERR is turned on, and lower 4 bits are shown
;===============================
	push	r17

	in	r17, LED_PORT
	cbr	r17, LED_MASK

	cpi	r16, 15
	brlo	led_show_number_4bit
	sbr	r17, LED_ERR

led_show_number_4bit:
	sbrc	r16, 0
	sbr	r17, LED_GREEN0

	sbrc	r16, 1
	sbr	r17, LED_RED0

	sbrc	r16, 2
	sbr	r17, LED_GREEN1
	
	sbrc	r16, 3
	sbr	r17, LED_RED1

	out	LED_PORT, r17
	
	pop	r17
	ret
; 2}}}

; LED_ON {{{2
LED_ON:
;===============================
; Turns on Led/Leds
;==============================
; INPUT: 
;	r17 - Leds to turn on
;===============================
	push	r16
	in	r16, LED_PORT
	or	r16, r17
	out	LED_PORT, r16
	pop	r16
	ret
; 2}}}

; LED_OFF {{{2
LED_OFF:
;===============================
; Turns off Led/Leds
;==============================
; INPUT: 
;	r17 - Leds to turn off
;===============================
	push	r16
	in	r16, LED_PORT
	or	r16, r17
	eor	r16, r17
	out	LED_PORT, r16
	pop	r16
	ret
; 2}}}

; LED_SWITCH {{{2
LED_SWITCH:
;===============================
; Switch on/off Led/Leds
;==============================
; INPUT: 
;	r17 - Leds to switch
;===============================
	push	r16
	in	r16, LED_PORT
	eor	r16, r17
	out	LED_PORT, r16
	pop	r16
	ret
; 2}}}
; 1}}}

;.eseg			; EEPROM section
;.dseg			; SRAM section

